# How to Git

## Que veremos en este training? Lo básico de Git.

#       ##1.-Inicializar un repo git
#       ##2.-Clonar un repositorio git existente
#       ##3.-Comitear una version modificada de un archivo del repositorio
#       ##4.-Configurar un repo git, para recibir colaboraciones desde un repositorio rmeoto
#       ##5.-Comandos git usados frecuentemente

```
#Nos ubicamos en la ruta dónde inicializaremos nuestro proyecto
1-.     cd /home/sxa391/howtogit
#Inicializaremos el repositorio
2.-     git init
#Creamos nuestro primer archivo: README.md
3.-     vi README.md
#Lo añadimos al repositorio localhost (Todo lo que añada con este comando, parasá a una etapa llamada staging.)
4.-     git add .
#Consultamos el status actual del repositorio local
5.-     git status
            new file:   README.md
#Luego ejecutaremos nuestro primer commit local.
6.-     git commit -m "Mi primer commit"
            [master (root-commit) 77953e3] Mi primer commit
            1 files changed, 20 insertions(+), 0 deletions(-)
#Crearemos en repositorio remoto con el mismo nombre del repositorio local
7.-     https://bitbucket.org

#Revisaremos nuestras configuraciones locales
8.-     git config -l
        git config --global user.name "Santiago Alamos"                 #Seteo el nombre de usuario para mis commit.
        git config --global user.email "santiago.alamos@equifax.com"    #Seteo el mail para mis commit.

#Subiremos nuestro repositorio local a nuestro nuevo repositorio remoto.
9.-     cd /home/sxa391/howtogit     #Nos ubicaremos en la ruta de nuestro repositorio local
        git remote -v                #Listo los repositorios remotos condfigurados (No deberiamos ver nada en primera instancia)
        git remote add origin https://salamos391@bitbucket.org/salamos391/howtogit.git    #Añadimosel nuevo repositorio remoto
              git remote -v     #Consultamos nuevamente los repositorios remotos configurados
                  origin  https://salamos391@bitbucket.org/salamos391/howtogit.git (fetch)
                  origin  https://salamos391@bitbucket.org/salamos391/howtogit.git (push)
        git push -u origin master #Subimos nuestros cambios al repositorio remoto (Nos pedirá credenciales de acceso)
              #Nota: Todo lo que yo quiero y/o necesito subir al repositorio remoto, tienes que estar comiteado en el repositorio local. De lo contrario no se subirá nada.
#Crearemos un archivo en el repositorio remoto, y luego haremos pull(lo descargaremos y uniremos) a nuestro repositorio local.
10.-   git pull                     #Descargaré mi repositorio remoto al local.

#Crearemos un nuevo branch en el repositorio local.
11.-   git branch my1stbranch     #Esta branch se creará desde el HEAD que tengamos seleccionado: En este caso Master
       mkdir 1stbranch ; cd 1stbranch ; touch 1st.txt 2nd.txt 3er.txt
       git add .
       git commit -m "Commit 1st branch"
       git push -u origin my1stbranch

#Seguiremos integrando cambios a nuestro branch my1stbranch
12.-   mkdir 1stbranch_too ; cd 1stbranch_too ; touch 1st.txt 2nd.txt 3er.txt
       git add .
       git commit -m "Añado folder 1stbranch_too"
       git push -u origin my1stbranch

#En la UI de BitBucket
13.-   pull request -> git merge      #La branch(rama) my1stbranch ya quedó integrada en la branch(rama) Master

#
```

#Otros comandos que nos pueden interesar:
```

git log            #Listar la historia de commits del proyecto.
git log -p <file>  #Listar la historia de commits de un archivo en especifico.
git blame <file>   #Listar Quién, qué y cuándo modificó un archivo.

git branch -av     #Listar todas las branch existentes en los repositorios local y remoto.
git branch -dr <remote/branch>  #Delete a branch on the remote.

```

#Nuevo proyecto para utilizar git clone
```
#Creamos la carpeta dónde clonaremos el proyecto.
1.-   cd /home/sxa391/webexample

#Para el git clone, tomar este caso como ejemplo.
#Clonamos la siguiente web
2.-   git clone https://github.com/daattali/beautiful-jekyll

```

